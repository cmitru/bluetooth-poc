import React, {useState, useEffect} from 'react';

import {
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
  Button,
  PermissionsAndroid,
} from 'react-native';

import {BleManager, Device} from 'react-native-ble-plx';
import {DeviceCard} from './Device';

const manager = new BleManager();

const App = () => {
  console.log('App');
  const [isScanning, setIsScanning] = useState(false);
  const [list, setList] = useState([]);

  const startScan = () => {
    manager.startDeviceScan(
      null,
      {
        allowDuplicates: false,
      },
      (error, device) => {
        console.log('Scanning...');
        setIsScanning(true);
        if (null) {
          console.log('null');
        }
        if (error) {
          manager.stopDeviceScan();
          return;
        }
        if (device && !list.find(dev => dev.id === device.id)) {
          setList([...list, device]);
        }
      },
    );
    setTimeout(() => {
      manager.stopDeviceScan();
      setIsScanning(false);
    }, 5000);
  };
  useEffect(() => {
    if (Platform.OS === 'android' && Platform.Version >= 23) {
      PermissionsAndroid.check(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      ).then(result => {
        if (result) {
          console.log('Permission is OK');
        } else {
          PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          ).then(result => {
            if (result) {
              console.log('User accept');
            } else {
              console.log('User refuse');
            }
          });
        }
      });
    }
    startScan();
    return () => {
      console.log('unmount');
    };
  }, [list]);

  return (
    <SafeAreaView>
      <View style={{margin: 10}}>
        <Button
          title={'Scan Bluetooth (' + (isScanning ? 'on' : 'off') + ')'}
          onPress={() => startScan()}
        />
        <Text style={styles.sectionTitle}>Number of devices {list.length}</Text>
        {list.length == 0 && (
          <View style={{flex: 1, margin: 20}}>
            <Text style={{textAlign: 'center'}}>No peripherals</Text>
          </View>
        )}
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            paddingBottom: 120,
          }}>
          {list.map((device, key) => {
            return <DeviceCard key={key} device={device} />;
          })}
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },

  sectionTitle: {
    textAlign: 'center',
    marginVertical: 10,
    fontSize: 20,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
  activityIndicatorContainer: {marginVertical: 6},
});

export default App;
