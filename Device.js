import React, {useEffect, useState, useCallback} from 'react';

import {StyleSheet, Text, Button, View, TouchableOpacity} from 'react-native';

const DeviceCard = ({device}) => {
  const [isConnected, setIsConnected] = useState(false);

  useEffect(async () => {
    device.isConnected().then(setIsConnected);

    // give a callback to the useEffect to disconnect the device when we will leave the device screen
    return () => {
      disconnectDevice();
    };
  }, [device]);
  const connectToDevice = async () => {
    try {
      console.log('Trying to connect to.... ', device.name);
      // discover all device services and characteristics
      const connectedDevice = await device.connect();
      setIsConnected(true);
      const allServicesAndCharacteristics =
        await connectedDevice.discoverAllServicesAndCharacteristics();
      console.log(
        'allServicesAndCharacteristics',
        allServicesAndCharacteristics,
      );
      // get the services only
      const discoveredServices = await allServicesAndCharacteristics.services();
      console.log('discoveredServices', discoveredServices[0]);
    } catch (err) {
      console.log('err', err);
    }
  };
  return (
    <TouchableOpacity
      style={[
        styles.container,
        isConnected
          ? {
              backgroundColor: 'teal',
            }
          : {
              backgroundColor: '#efefef',
            },
      ]}
      onPress={connectToDevice}>
      <Text>{`Id : ${device.id}`}</Text>
      <Text>{`Name : ${device.name}`}</Text>
      <Text>{`Is connected : ${isConnected}`}</Text>
      <Text>{`RSSI : ${device.rssi}`}</Text>
      <Text>{`UUIDS : ${device.serviceUUIDs}`}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    marginBottom: 12,
    borderRadius: 16,
    backgroundColor: '#efefef',
    padding: 12,
  },
});

export {DeviceCard};
